package chapter13;

public abstract class GeometricObject {
    private String color;
    private boolean filled;
    private java.util.Date dateCreated;
    // construct default geometric object
    protected GeometricObject(){
        dateCreated = new java.util.Date();
    }
    //construct geometric object with color and filled value
    protected GeometricObject(String color, boolean filled){
        dateCreated = new java.util.Date();
        this.color = color;
        this.filled = filled;
    }

    // return color

    public String getColor(){
        return color;
    }
    // set a new color
    public  void  setColor(String color){
        this.color = color;
    }
    // return filled since filed is boolean the getter method is isfilled
    public boolean isFilled(){
        return filled;
    }
    // set new filled
    public void setFilled(boolean filled){
        this.filled = filled;
    }
    // gate date created
    public java.util.Date getDateCreated(){
        return dateCreated;
    }

    public String toString(){
        return " created on " + getDateCreated()+ "\n color: "+ color + " and filled: "+ filled;
    }
    // abstracted method getArea
    public abstract double getArea();
    // abstract method getPerimeter
    public abstract double getPerimeter();
    public abstract boolean equals(Object o);

    public  int compareTo(GeometricObject o){
        if (getArea() > o.getArea()){
            return -1;
        } else if(getArea()< o.getArea()){
            return -1;
        }else {
            return 0;
        }
    }
    public static GeometricObject max(GeometricObject object1, GeometricObject object2, GeometricObject object3) {
        if (object1.compareTo(object2) > 0) {
            return object1;
        }else if (object2.compareTo(object3)> 0){
            return object3;
        }else {
            return object2;
        }
    }
}