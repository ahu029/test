package chapter13;

 class testGeometricObject {
     /** Main method */
     public static void main(String[] args) {
         // Declare and initialize two geometric objects
         GeometricObject g1 = new Circle(1);
         GeometricObject g2 = new Circle(2);
         GeometricObject g3 = new Rectangle(5, 3);
         GeometricObject g4 = new Rectangle(3, 8);
         GeometricObject g5 = new Triangle(1, 3, 4);
         GeometricObject g6 = new Triangle(8, 9, 9);
         GeometricObject g7;
         GeometricObject g8;
         GeometricObject g9;

         System.out.println("The biggest circle:");
         if (g1.getArea() < g2.getArea()) {
             System.out.println(g2.toString());
             g7 = g2;
         }
         else {
             System.out.println(g1.toString());
             g7 = g1;
         }

         System.out.println();

         System.out.println("The biggest rectangle:");
         if (g3.getArea() < g4.getArea()) {
             System.out.println(g4.toString());
             g8 = g4;
         }
         else {
             System.out.println(g3.toString());
             g8 = g3;
         }

         System.out.println();

         System.out.println("The biggest triangle:");
         GeometricObject geoObject9;
         if (g5.getArea() < g6.getArea()) {
             System.out.println(g6.toString());
             geoObject9 = g6;
         }

         else {
             System.out.println(g5.toString());
             geoObject9 = g5;
         }

         System.out.println();

         System.out.print("The biggest geometric object is ");
         if (g7.getArea() < g8.getArea()) {
             if (g8.getArea() < geoObject9.getArea()) {
                 System.out.println("triangle:");
                 System.out.println(geoObject9.toString());
             }
             else {
                 System.out.println("rectangle:");
                 System.out.println(g8.toString());
             }
         }
         else {
             if (g7.getArea() < geoObject9.getArea()) {
                 System.out.println("triangle:");
                 System.out.println(geoObject9.toString());
             }
             else {
                 System.out.println("circle:");
                 System.out.println(g7.toString());
             }
         }
     }
 }








