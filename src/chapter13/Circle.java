package chapter13;

public class Circle extends GeometricObject {
    private double radius;

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }
    public double getRadius() {
        return radius;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return radius * radius * Math.PI;
    }
    public double getDiameter() {
        return 2 * radius;
    }
    @Override
    public double getPerimeter() {
        return 2 * radius * Math.PI;
    }

    /* Print the circle info */
    /*public void printCircle() {
        System.out.println(" radius:" + getRadius())+ "\ncreated  date" + getDateCreated());}*/

    public String toString(){
        return  " Radius:" + getRadius() + "\n Created date: "+ getDateCreated();
    }
    public boolean equals(Object o) {
        if (this != o) {
            return false;
        }
        else {
            return true;
}
    }
}