package chapter13;

import java.util.GregorianCalendar;
import java.util.Scanner;

public class Car extends Vehicle {
    private int power;
    private GregorianCalendar productionDate;
    Car(){
        productionDate = new GregorianCalendar();
    }
    public Car( String name, String serialNumber, String color, int price, int power, int model, int direction, double speed){
        productionDate = new GregorianCalendar();
        this.power = power;
        setSerialNumber(serialNumber);
        setColor(color);
        setDirection(direction);
        setModel(model);
        setPrice(price);
        setSpeed(0);
        setName(name);
    }

    public Car(String black, String volvo, String s, int i, int i1, int i2, int i3) {
    }

    @Override

    public void setAllField(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the car's fields:");
        System.out.print("Colour: ");
        setColor(input.nextLine());
        System.out.print("Name: ");
        setName(input.nextLine());
        System.out.print("SerialNumber: ");
        setSerialNumber(input.nextLine());
        System.out.print("Model: ");
        setModel(input.nextInt());
        System.out.print("Price: ");
        setPrice(input.nextInt());
        System.out.print("Power: ");
        power = input.nextInt();
        setDirection(0);
        setSpeed(0);
    }
    @Override
    public void turnLeft(int degrees){
        if (0 <= degrees && degrees <= 360){
            setDirection(getDirection() - degrees);
            if (getDirection() < 0)
                setDirection(getDirection()-360);
    }
    }
    @Override
    public void turnRight(int degrees){
            if (0 <= degrees && degrees <= 360){
                setDirection(getDirection() + degrees);
                if (getDirection() >= 360)
                    setDirection(getDirection()+360);
            }
    }
    public int getPower(){
        return power;
    }
    public GregorianCalendar getProductionDate(){
        return productionDate;
    }
    public void setPower( int power){
        this.power = power;
    }
    public void setProductionDate(GregorianCalendar productionDate){
        this.productionDate = productionDate;
    }
    @Override
    public String toString(){
        return " power: " +getPower()+ " , production date: "+ getProductionDate();
    }

    @Override
    public Object turnLeft() {
        return null;
    }
}

