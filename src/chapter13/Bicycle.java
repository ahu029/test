package chapter13;

import java.util.GregorianCalendar;
import java.util.Scanner;

public class Bicycle extends Vehicle {
    private int gears;
    private GregorianCalendar productionDate;
    Bicycle(){
    }
    public Bicycle(String name, String serialNumber, String color, int price, int gears, int model, int direction, double speed){
        this.gears = gears;
        setSerialNumber(serialNumber);
        setColor(color);
        setDirection(direction);
        setModel(model);
        setPrice(price);
        setSpeed(0);
        setName(name);
    }

    public Bicycle(String yellow, String blue, String bc100, int i, int i1, int i2, int i3) {

    }

    @Override
    public void setAllField(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the bicyle's fields:");
        System.out.print("Colour: ");
        setColor(input.nextLine());
        System.out.print("Name: ");
        setName(input.nextLine());
        System.out.print("Serial Number: ");
        setSerialNumber(input.nextLine());
        System.out.print("Model year: ");
        setModel(input.nextInt());
        System.out.print("Price: ");
        setPrice(input.nextInt());
        System.out.print("Gear: ");
        gears = input.nextInt();
        setDirection(0);
        setSpeed(0);
    }

    @Override
    public void turnLeft(int degrees) {
        System.out.println("The bicycle turned " + degrees + " degrees to the left");
    }

    @Override
    public void turnRight(int degrees) {
        System.out.println("The bicycle turned " + degrees + " degrees to the left");
    }

    public int getGears(){
        return gears;
    }

    public GregorianCalendar getProductionDate() {
        return productionDate;
    }
    public void setGears( int gears){
        this.gears = gears;
    }
    public void setProductionDate(GregorianCalendar productionDate){
        this.productionDate = productionDate;
    }
    @Override
    public String toString(){
        return " number of gears: "+ getGears()+ " production date: " + getProductionDate();
    }

    @Override
    public Object turnLeft() {
        return null;
    }
}
