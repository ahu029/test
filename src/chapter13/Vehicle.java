package chapter13;

import java.util.GregorianCalendar;

public abstract class Vehicle {
    private  String name;
    private  String color;
    private  String serialNumber;
    private  int model;
    private  int price;
    private  int  direction;
    private double speed;
    protected java.util.Scanner input;
    private GregorianCalendar productionDate;
    Vehicle(){
        productionDate = new GregorianCalendar();;
    }
    public Vehicle(String name, String color, String  serialNumber, int model, int price, int direction, double speed){
        productionDate = new GregorianCalendar();;
        this.color = color;
        this.direction = direction;
        this.name = name;
        this.price = price;
        this.model = model;
        this.serialNumber = serialNumber;
        speed = 0;
    }
    public void setAllField(){
        color = input.nextLine();
        name = input.nextLine();
        serialNumber = input.nextLine();
        model = input.nextInt();
        price = input.nextInt();
        direction = 0;
        speed = 0;
    }
    public abstract void turnLeft(int degrees);
    public abstract void turnRight(int degrees);


    public String getName(){
        return name;
    }
    public String getColor(){
        return  color;
    }
    public String getSerialNumber(){
        return serialNumber;
    }
    public int getModel(){
        return  model;
    }
    public int getPrice(){
        return price;
    }
    public int getDirection(){
        return direction;
    }
    public double getSpeed(){
        return speed;
    }

    public GregorianCalendar getProductionDate() {
        return productionDate;
    }

    public void setName(String name){
        this.name = name;
    }
    public void setColor(String color){
        this.color = color;
    }
    public void setSerialNumber( String  serialNumber){
        this.serialNumber = serialNumber;
    }
    public void  setModel(int model){
        this.model =model;
    }
    public void  setPrice(int price){
        this.price = price;
    }
    public void setDirection(int direction){
        this.direction = direction;
    }
    public void setSpeed(double speed){
        this.speed = speed;
    }
    public void setProductionDate(GregorianCalendar productionDate){
        this.productionDate = productionDate;
    }
    public String toString(){
        return  "Color:"+ getColor()+ " name: "+ getName()+ " Setial number: "+ getSerialNumber()+ " date of production: " + getSerialNumber();
    }

    public abstract Object turnLeft();
}
