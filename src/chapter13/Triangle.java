package chapter13;

public class Triangle extends GeometricObject {
    private double side1;
    private double side2;
    private double side3;
     Triangle(){
         side1 = 1;
         side2 = 1;
         side3 = 1;
     }
    Triangle(double sides){
        side1 = sides;
        side2 = sides;
        side3 = sides;
     }
    public Triangle(double side3, double side1, double side2){
      this.side1 = side1;
      this.side2 = side2;
      this.side3 = side3;
    }
    public Triangle(double side1, double side2, double side3, boolean filled, String color){
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
        setColor(color);
        setFilled(filled);
    }

    public double getSide1() { return side1; }

    public void setSide1(double side1) { this.side1 = side1; }

    public double getSide2() { return side2; }

    public void setSide2(double side2) { this.side2 = side2; }

    public double getSide3() { return side3; }

    public void setSide3(double side3) {
        this.side3 = side3;
    }
    @Override
    public double getPerimeter(){
         return side1 + side3 + side2;
    }
    public double getArea() {
        double m = (getPerimeter() / 2);
        return Math.pow(m * (m - side1) * (m - side2) * (m - side3), 0.5);
    }
    public String toString(){
        return "sides"+ getSide1() + " , " + getSide2() + " , " + getSide3()+ "\n date  " + getDateCreated();
    }
    @Override
    public boolean equals(Object o) {
        if (this != o) {
            return false;
        }
        else {
            return true;
        }
    }
      //@Override
    boolean checkLegality(double side1, double side2, double side3) {
        if (side1 < 0 || side2 < 0 || side3 < 0) {
            return false;
        }
        if (side2 + side3 <= side1 || side1 <= Math.abs(side3 - side2)) {
            return false;
        }
        else {
            return true;
        }
        }
}

